import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import CreateRoom from '@/components/CreateRoom'
import Rule from '@/components/Rule'
import Room from '@/components/Room'
import HowToUse from '@/components/HowToUse'
import Inquiry from '@/components/Inquiry'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/create_room',
      name: 'CreateRoom',
      component: CreateRoom
    },
    {
      path: '/room/:room_id',
      name: 'Room',
      component: Room,
      props: true
    },
    {
      path: '/rule',
      name: 'Rule',
      component: Rule
    },
    {
      path: '/how-to-use',
      name: 'HowToUse',
      component: HowToUse
    },
    {
      path: '/inquiry',
      name: 'Inquiry',
      component: Inquiry
    }
  ]
})
